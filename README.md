**[Jason S. Fillman](https://www.linkedin.com/in/jasonfillman) - Solutions Architect - [jason@jasonfillman.com](mailto://jason@jasonfillman.com)**

### Skills:
* Over 20 years of Unix/Linux systems administration experience
* Experience supporting faculty and students in a research university environment
* Extensive consulting and customer support experience
* Deep knowledge of DevOps tools and processes
* Experience migrating systems to cost-effective cloud solutions
* Familiarity with configuration tools such as Ansible, Puppet, and Kicktstart.
* Experience with secure authentication standards including SAML, LDAP, OAuth, and Kerberos
* Ability to quickly learn and adapt to new tools and processes
* Goal-oriented and highly motivated

### Experience:
#### SOLUTIONS ARCHITECT - GITHUB, SAN FRANCISCO, CA - 2019-CURRENT
* Serves as a long-term trusted advisor to Fortune 100 companies
* Designs enterprise scale developer solutions centered around the GitHub platform
* Advises on security vulnerability resolution and client pentest responses
* Gathers customer requirements and advises on solutions to improve development lifecycles and code quality
* Educates clients on new security features such as:
  * Vulnerability detection
  * Token scanning
  * Code analysis ([Semmle](https://semmle.com/))
* Provides consultation on custom integrations and API usage
* Serves as subject matter expert on Atlassian integration and best practices

#### SOLUTIONS ARCHITECT - CLEARVISION CM, SAN FRANCISCO, CA — 2016-2018
* Lead Clearvision's West Coast-based Solutions Engineering team
* Designed and implemented advanced solutions and processes, based on client business requirements including:
	* Requirements gathering (discovery)
	* Highly available hardware and network designs
	* Cloud infrastructure
	* Application configuration
	* Design documentation
* Introduced new security-focused offerings to customers including:
	* System and network security testing for developer tools
	* Designing security incident response procedures and workflows
	* Integrating new tools with existing SIEM tools like RSA Archer and Securonix
* Deployed tools on numerous platforms including:
	* Amazon Web Services (AWS) via Cloudformation
	* Microsoft Azure
	* VMWare vSphere
	* Containers (Docker, Kubernetes, etc.)
	* Bare Metal
* Served as subject matter expert for:
	* High Availability and Disaster Recovery
	* Public Cloud Deployments
	* SAML Authentication
	* LDAP and Active Directory integrations
	* Linux administration
	* System security
* Provided training to both clients and new Clearvision employees on:
	* JIRA administration
	* Advanced JIRA workflows
	* Bitbucket administration
	* Advanced git
* Assisted Atlassian in the creation of their [ACP-500 Atlassian System Administrator](https://www.atlassian.com/university/certification/certifications/exam-acp-500) exam
* Acquired certification as both a [JIRA Administrator](https://www.certmetrics.com/atlassian/public/badge.aspx?i=1&t=c&d=2017-10-17&ci=AT00131508) and [Atlassian System
	Administrator](https://www.certmetrics.com/atlassian/public/badge.aspx?i=5&t=c&d=2017-04-25&ci=AT00131508)

#### DEVELOPER TOOLS ENGINEER - MACHINE ZONE INC, PALO ALTO, CA — 2014-2016
* Founded and grew the Internal Dev Tools team to a team of 3 engineers
* Took ownership of all core developer tools including:
	* Git (BitBucket Data Center, GitLab, GitHub Enterprise, Phabricator)
	* Perforce
	* Bug Tracking (JIRA)
	* Wiki (Confluence)
* Designed and built scalable HA clusters, built on:
	* Atlassian Data Center products
	* Postgres + Repmgr + Barman
	* Tegile storage
	* F5 load balancers
* Collaborated with the Site Reliability (SRE) team to migrate all production systems between data centers
  * Migrated all developer tools with minimal downtime
  * Updated Puppet configurations to speed migrations
  * Assisted in migrating production client-facing systems (1000+ physical servers)
* Woked closely with IT to bind the Dev Tools to their growing array of auth sources, including:
	* OpenLDAP
	* Active Directory
	* Okta / SAML
	* Atlassian Crowd
* Assisted Data Infrastructure in rolling out and/or maintaining solutions including:
  * Tableau
  * Vertica
  * Apache Kafka
* Consulted with various teams to recommend streamlined products and solutions for:
  * Development teams
  * Research teams
  * Data science
  * HR
  * Facilities
* Led Agile adoption for Operations, including coordinating Scrum training, and release planning
* Provided release planning training, utilizing JIRA Portfolio
* Migrated IT team to JIRA Service Desk from SolarWinds Web Help Desk
* Standardized version control on BitBucket Data Center
* Built backup and misc scripting with standard UNIX tools (Bash, Sed, Awk, etc.)
* Automated systems deployment and configuration via Ansible


#### SR. TOOLS DEVELOPER - GOOD TECHNOLOGY; SUNNYVALE, CA — 2012-2014
* Owned administration of all core developer and collaboration tools
* Installed, managed, and configured the entire suite of Atlassian’s on-premise products
* Maintained test case management (TestRail) and Agile planning (JIRA Agile) tools
* Integrated Active Directory and Crowd SSO authentication into all Tools servers and services
* Automated routine tasks and deployments using Ansible
* Built backup scripts etc. using basic Bash, Python, and Ansible playbooks
* Delivered hardware requirements and highly competitive pricing for new projects to Purchasing
* Worked with Network Security to harden systems against vulnerabilities using Qualys
* Designed and maintained high-availability, backup, and disaster recovery solutions
* Connected applications from different vendors using REST and JSON
* Deployed and maintained development Oracle 11g systems
* Provided solutions to end-user questions and requests
* Performed physical to virtual migration of all hardware using Ubuntu and KVM
* Designed, purchased, and implemented new software-defined storage system
  * Utilized high performance, affordable off the shelf hardware
  * Avoided vendor lock-in by using open standards including ZFS, Direct NFS, SMB, and iSCSI
  * Monitored and tuned performance between cache, solid state, and spinning disks
* Built extremely low-latency storage network using Arista 10GbE hardware
  * Performed all initial configuration in Arista eOS
  * Optimized networking speed and reliability through multi-chassis ling aggregation (MCLAG)
* Designed and implemented tools for system monitoring and analysis
  * Splunk server for log and performance analysis
  * Custom Splunk dashboards for relevant system metrics
  * New Relic for performance and offsite monitoring/reporting
  * Zabbix for standard system monitoring and alerting
* Facilitated initiative to put “Everything in Git” (configs, automation, etc.) from legacy VCS and build systems to Bitbucket and Bamboo
* Designed proof of concept private cloud, based on OpenStack, to replace legacy VMWare Lab Manager


#### SYSTEMS ANALYST - DEPARTMENT OF ASTRONOMY - UNIVERSITY OF TEXAS; AUSTIN, TX — 2008-2012
* Supported the Astronomy Department and McDonald Observatory's network of Linux (RHeL), Solaris, and OS X Server systems
* Consulted with Astromomy faculty during the first semester of the popular [Freshman Research Initiative](https://cns.utexas.edu/fri):
  * Reviewed and tested course materials
  * Configured and deployed lab systems to facilitate new initiatives
  * Attended FRI classes to provide realtime support to faculty and students
* Implemented backup and synchronization of research data between the primary UT Austin campus and McDonald Observatory
  * Data included student observation runs and [HETDEX](http://www.hetdex.org) data
* Supported high-performance computation nodes used by the [Computational Astrophysics Group](https://astronomy.utexas.edu/research/research-areas/theory)
* Deployed high performance Xgrid cluster and job queuing to take advantage of lab hardware for research
* Designed and deployed LDAP authentication and NFS home directories for classroom and lab computers
* Proactively scanned and secured systems from continuous outside attacks
* Modernized system deployment methods to use imaging and configuration management
* Helped faculty and students compile specialized research software on their workstations and servers
* Optimized Python environments for research via NumPy, SciPy, PyRAF, and other packages
* Developed solutions with Python for daily admin tasks
* Made new hardware purchases and recommendations for the departmental IT group
* Promoted use of resources from the [Texas Advanced Computing Center](https://www.tacc.utexas.edu) when appropriate
* Tracked and resolved end-user requests with the RT: Request Tracker ticketing system


#### MAC GENIUS, APPLE RETAIL - THE DOMAIN; AUSTIN, TX — 2008
* Provided excellent technical software and hardware support for Apple end-user customers
* Used prior Unix knowledge to resolve difficult low-level software issues
* Performed certified warranty repairs on Apple products
* Maintained a Net Promoter (positive review) score of over 90%
* Providing feedback to Apple Engineering on emerging issues
* Gave technical consultation to the Specialist and Concierge teams
* Maintained a positive rapport with the Specialists, Concierges, and management team


#### IPHONE SUPPORT, APPLE INC.; AUSTIN, TX — 2007
* Effectively served in the Tier 2 iPhone queue during the first week of product launch
* Maintained an overall customer satisfaction rating of 97% and call resolution of 90%
* Kept escalations under 4%, resolving most issues on the first call
* Exceeded expectations on a regular basis by following up calls with emails to customers
* Represented the customer during conference calls with AT&T, and everything within my power to fully resolve the issue

The latest version of this resume is always available on [Bitbucket](https://bitbucket.org/jsfillman/resume/overview).